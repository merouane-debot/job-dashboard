import datetime
import re
import pandas as pd
from offres_emploi import Api

def api_pe(*args):
    """
    La fonction requete l'API pole emploi afin d'obtenir les offres d'emploi disponible avec
    le mots clé data Output : CSV des offres d'emploi disponible
    """

    client = Api(client_id="votre clé",
             client_secret="votre clé")

    data = {
        "intitule":[],
        "nom_entreprise": [],
        "lieu": [], "date": [],
        "lien":[],
        "description":[]
    }

    for word in args:

        params = {
            "motsCles": word,
            "region": 84,
            "sort" : 1
        }

        basic_search = client.search(params=params)

        for i in range(len(basic_search['resultats'])):
            data["intitule"].append(basic_search['resultats'][i]['intitule'])

            lieu = basic_search['resultats'][i]['lieuTravail']['libelle']
            matching = re.search(r'- (?P<lieu>\w+)', lieu)
            lieu = matching.group(1)
            data["lieu"].append(lieu.lower())

            date = basic_search['resultats'][i]['dateCreation']
            date = datetime.datetime.strptime(date, ('%Y-%m-%dT%H:%M:%S.%f%z'))

            data["date"].append(date.date())
            data["lien"].append(basic_search['resultats'][i]['origineOffre']["urlOrigine"])
            data["description"].append(basic_search['resultats'][i]['description'])

            if "nom" in basic_search['resultats'][i]['entreprise']:
                data["nom_entreprise"].append(basic_search['resultats'][i]['entreprise']["nom"])
            else:
                data["nom_entreprise"].append("Unknow")

    df = pd.DataFrame(data)
    df.to_csv("../csv/pe.csv", index =False)

    return df
