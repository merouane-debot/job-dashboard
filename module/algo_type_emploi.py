import operator

def get_type_poste(name_poste):
    """
    La fonction return une estimation du metier en fonction de l'intituler de
    l'offre passé en parametre.
    intput : intitulé d'une offre d'emploi --> type string
    output : type de poste --> type string
    """

    categorie = {
        "Data Scientist":[("scientist", 5), ("machine learning", 3), ("predictive", 4), ("modeling", 2), ("big data", 2)], 
        "Data Analyst": [("analyst", 5), ("analyse",5), ("visualization", 4), ("machine learning", 2) , ("detective", 3), ("business intelligence", 5), ("bi ", 5), ("buisness", 5), ("analytics", 4), ("bi,", 5)], 
        "Data Architect": [("architect", 5), ("data modeller", 4), ("etl", 3), ("modeling", 4), ("architecture", 5), ("warehouse", 4), ("warehousing",4), ("lake", 4), ("hadoop",4), ("big data", 3)], 
        "Data Engineer": [("engineer", 5), ("ingénieur data", 5), ("data ingénieur", 5), ("etl",4), ("modeling", 3), ("api",3), ("warehouse",3), ("warehousing", 3), ("architecture", 3)],
        "Statistician":[("statistician", 5), ("mining", 2), ("hadoop",2), ("machine learning", 3), ("statistic",5), ("analyze", 4), ("statistical",5), ("analyse", 4)], 
        "Administrator":[("administrator", 5), ("data base", 3), ("security", 5), ("sécurité", 5), ("hadoop", 2), ("erp",4), ("backup",4), ("recovery",4), ("cloud", 3), ("réseaux", 4), ("systèmes", 4), ("systems", 4), ("administrateur",5)],  
        "Data Manager": [("manager", 5), ("team", 4), ("leader",4), ("leadership",4), ("communication",3), ("predictive", 2), ("project", 4), ("management", 5), ("chef de projet data", 5), ("lead", 4), ("governance", 4)]
    }

    type_poste = dict()

    for key in categorie.keys():
        for i in range(len(categorie[key])):
            count=0
            if categorie[key][i][0] in name_poste.lower():
                count += categorie[key][i][1]
                type_poste[key] = count
            else:
                type_poste["Autres"]=0

    return(max(type_poste.items(), key=operator.itemgetter(1))[0])
